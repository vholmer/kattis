# Kattis
Solving problems on kattis in various languages.

Problem dirs are named SOLVED_{name} if they are solved, if not they are named simply {name}.

Problems can be found on https://open.kattis.com/problems/{name}.

# Unsolved WIP Problems
Collidingtraffic, nnnnn. Will use correct branching strategy per problem in the future and merge with master only when completed.