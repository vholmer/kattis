#include <stdio.h>

typedef struct c_stack {
	char data[10];
	const char* head;
	char* sp;
} c_stack;

typedef struct i_stack {
	int data[10];
	const int* head;
	int* sp;
} i_stack;

void c_push(c_stack* s, char n) {
	*((s->sp)++) = n;
}

char c_pop(c_stack* s) {
	return *(--(s->sp));
}

char c_hasData(c_stack* s) {
	if((s->sp)-1 < s->head)
		return 0;
	return 1;
}

char c_peek(c_stack* s) {
	return *((s->sp)-1);
}

void i_push(i_stack* s, int n) {
	*((s->sp)++) = n;
}

int i_pop(i_stack* s) {
	return *(--(s->sp));
}

int i_hasData(i_stack* s) {
	if((s->sp)-1 < s->head)
		return 0;
	return 1;
}

void parse(char* expr, char* q) {
	c_stack chars;
	chars.head = chars.data;
	chars.sp = chars.data;
	int cur = 0;
	int out = 0;
	char token;
	while(cur < 7) {
		token = expr[cur];
		if(token == '4') {
			q[out++] = token;
		} else {
			while(c_hasData(&chars)) {
				switch(token) {
					case '*': {
						if(c_peek(&chars) == '*' || c_peek(&chars) == '/') {
							q[out++] = c_pop(&chars);
						}
						goto out;
					}
					case '+': q[out++] = c_pop(&chars); break;
					case '-': q[out++] = c_pop(&chars); break;
					case '/': {
						if(c_peek(&chars) == '*' || c_peek(&chars) == '/') {
							q[out++] = c_pop(&chars);
						}
						goto out;
					}
				}
			}
		out:
			c_push(&chars, token);
		}
		cur++;
	}
	while(c_hasData(&chars)) {
		q[out++] = c_pop(&chars);
	}
}

int evalRPN(char* expr) {
	i_stack nums;
	nums.head = nums.data;
	nums.sp = nums.data;
	int i, n, a, b;
	char token;
	for(i = 0; i < 7; ++i) {
		token = expr[i];
		if(token == '4') {
			i_push(&nums, 4);
		} else {
			a = i_pop(&nums);
			b = i_pop(&nums);
			switch(token) {
				case '*': i_push(&nums, b * a); break;
				case '+': i_push(&nums, b + a); break;
				case '-': i_push(&nums, b - a); break;
				case '/': {
					if(a == 0) {
						return 0;
					}
					i_push(&nums, b / a);
					break;
				}
			}
		}
	}
	int res = i_pop(&nums);
	return res;
}

void spacePrint(const char* expr, const int res) {
	for(int i = 0; i < 7; ++i) {
		if((i + 1) % 2 == 0) {
			printf(" %c ", expr[i]);
		} else {
			printf("%c", expr[i]);
		}
	}
	printf(" = %d\n", res);
}

void findSolution(int n) {
	char expr[7] = "4_4_4_4";
	char post[7];
	char ops[4] = "*+-/";
	int i, j, k, l, res = 0;
	if(n >= -60 && n <= 256) {
		for(i = 0; i < 4; ++i) {
			for(j = 0; j < 4; ++j) {
				for(k = 0; k < 4; ++k) {
					expr[1] = ops[i];
					expr[3] = ops[j];
					expr[5] = ops[k];
					parse(expr, post);
					res = evalRPN(post);
					if(res == n) {
						spacePrint(expr, res);
						return;
					}
				}
			}
		}
	}
	printf("no solution\n");
}

int main(void) {
	int lines, inp, readLines = 0;
	scanf("%d", &lines);
	while(readLines != lines) {
		scanf("%d", &inp);
		findSolution(inp);
		readLines++;
	}
	
	return 0;
}
