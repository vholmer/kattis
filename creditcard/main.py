import math
from decimal import *

# All values in units of 1 / 1000 dollars.

def rounds(x):
	return Decimal(x).quantize(0, rounding=ROUND_HALF_UP)

def roundCent(x):
	return rounds(x / 10) * 10

def applyRate(balance, rate):
	return roundCent(balance * (1 + (rate / 100)))

def main():
	years = 100
	limit = 12 * years
	n = int(input())
	for i in range(n):
		payments = 0
		line = input().split()
		rate = Decimal(line[0])
		balance = Decimal(line[1]) * 1000
		monthly = Decimal(line[2]) * 1000
		amountDue = balance
		impossible = False
		if applyRate(balance, rate) - balance >= monthly:
			impossible = True
		while(amountDue > 0 and payments <= limit and not impossible):
			amountDue = applyRate(amountDue, rate)
			amountDue -= monthly
			payments += 1
		if payments <= limit and not impossible:
			print(payments)
		else:
			print("impossible")

if __name__ == "__main__":
	main()