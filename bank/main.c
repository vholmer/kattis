#include <stdio.h>
#include <string.h>

typedef struct {
	int initialized;
	int money;
	int patience;
} Customer;

int main(void) {
	int numberOfCustomers, timeToClosing;
	scanf("%d%d", &numberOfCustomers, &timeToClosing);

	Customer serveArray[timeToClosing];
	Customer curCustomer;

	memset(&serveArray, 0, timeToClosing * sizeof(Customer));

	for (int i = 0; i < numberOfCustomers; ++i) {
		curCustomer.initialized = 1;
		scanf("%d%d", &curCustomer.money, &curCustomer.patience);
		int setTime = curCustomer.patience;
		while (setTime >= 0) {
			if (setTime >= timeToClosing) {
				setTime--;
				continue;
			}
			if (serveArray[setTime].initialized == 0) {
				serveArray[setTime] = curCustomer;
				break;
			} else if (curCustomer.money > serveArray[setTime].money) {
				Customer prevCustomer = serveArray[setTime];
				serveArray[setTime] = curCustomer;
				curCustomer = prevCustomer;
			}
			setTime--;
		}
	}

	int total = 0;
	for (int i = 0; i < timeToClosing; ++i)  {
		total += serveArray[i].money;
	}

	printf("%d", total);

	return 0;
}