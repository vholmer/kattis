package main

import (
	"bufio"
	"fmt"
	"os"
	"sort"
	"strconv"
	"strings"
)

func main() {
	scanner := bufio.NewScanner(os.Stdin)
	n := -1
	list_n := 0
	for {
		scanner.Scan()
		d := make(map[string]int)
		n, _ = strconv.Atoi(scanner.Text())
		if n == 0 {
			break
		}
		for i := 0; i < n; i++ {
			scanner.Scan()
			animalWords := scanner.Text()
			animalWords = strings.ToLower(animalWords)
			animalSplit := strings.Split(animalWords, " ")
			animal := animalSplit[len(animalSplit)-1]
			if _, ok := d[animal]; ok {
				d[animal] += 1
			} else {
				d[animal] = 1
			}
		}
		list_n += 1
		fmt.Printf("List %d:\n", list_n)
		sort_keys := make([]string, 0, len(d))

		for k, _ := range d {
			sort_keys = append(sort_keys, k)
		}

		sort.Strings(sort_keys)

		for i := 0; i < len(sort_keys); i++ {
			fmt.Printf("%s | %v\n", sort_keys[i], d[sort_keys[i]])
		}
	}
}
