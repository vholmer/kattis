import sys

def main():
	list_n = 0
	while(True):
		n = int(input())
		if n == 0:
			break
		d = {}
		for i in range(n):
			animal = input().split()[-1].lower()
			if animal in d:
				d[animal] += 1
			else:
				d[animal] = 1
		list_n += 1
		print("List {n}:".format(n = list_n))
		for key in sorted(d.keys()):
			print("{k} | {n}".format(k = key, n = d[key]))

if __name__ == "__main__":
	main()