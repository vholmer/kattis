def divide(x, d):
	res = ""
	rest = ""
	if d == 0:
		return res, rest
	for i in x:
		num = int(rest + i)
		res += str(int(num / d))
		rest = str(num % d)
	ind = 0
	for i in res:
		if i == "0":
			ind +=1
		else:
			break
	return res[ind:], rest

def calc(x):
	fi = x
	delta = len(fi)
	if delta >= 1000000:
		return ""
	alpha, rest = divide(fi, delta)
	omeg = len(alpha)
	while(True):
		prevDelta = delta
		n_alpha, n_rest = divide(fi, omeg)
		if alpha != n_alpha:
			delta -= 1
			alpha, rest = divide(fi, delta)
			omeg = len(alpha)
		elif alpha == n_alpha and rest == "0":
			break
		if prevDelta == delta or delta == 0:
			alpha = ""
			break
	return alpha

def main():
	n = input()
	if n == "0":
		print(n)
	else:
		print(calc(n))

if __name__ == '__main__':
	main()

# TODO: Implementera detta i C++ eller C istället för enorm speedup. Testa hastighet på data.in.
# Det tar 13 sekunder att processa data.in