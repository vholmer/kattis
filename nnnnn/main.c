#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#define MAX_LENGTH 1000000

typedef struct {
	char result[MAX_LENGTH];
	char rest[MAX_LENGTH];
} Tuple;

Tuple divide(char* x, int d) {
	Tuple t;
	strcpy(t.result, "");
	strcpy(t.rest, "");
	printf("4\n");
	int x_len = strlen(x);
	printf("5\n");
	if(d == 0)
		return t;
	char str[MAX_LENGTH];
	for(int i = 0; i < x_len; ++i) {
		strcpy(str, "");
		strcat(str, t.rest);

		char a[2];
		a[0] = x[i];
		a[1] = '\0';

		strcat(str, a);
		int num = atoi(str);

		strcpy(str, "");
		sprintf(str, "%d", num / d);
		strcat(t.result, str);

		strcpy(str, "");
		sprintf(str, "%d", num % d);
		strcpy(t.rest, str);
	}

	int ind = 0;
	int res_len = strlen(t.result);
	for(int i = 0; i < res_len; ++i) {
		if((char)t.result[i] == '0') {
			ind++;
		}
		else
			break;
	}
	memcpy(t.result, &t.result[ind], res_len - ind);
	t.result[res_len - ind] = '\0';
	return t;
}

void calc(char** x, char** dest) {
	printf("1\n");
	int delta = strlen(*x);
	if(delta >= MAX_LENGTH) {
		strcpy(*dest, "");
		return;
	}
	printf("2\n");
	Tuple t = divide((char*)&x, delta);
	printf("3\n");
	char alpha[MAX_LENGTH];
	strcpy(alpha, t.result);
	char rest[MAX_LENGTH];
	strcpy(rest, t.rest);
	int omeg = strlen(alpha);
	while(1) {
		int prevDelta = delta;
		t = divide((char*)&x, omeg);
		char n_alpha[MAX_LENGTH];
		strcpy(n_alpha, t.result);
		char n_rest[MAX_LENGTH];
		strcpy(n_rest, t.rest);
		if(strcmp(alpha, n_alpha) != 0) {
			delta--;
			t = divide((char*)&x, delta);
			strcpy(alpha, t.result);
			strcpy(rest, t.rest);
			omeg = strlen(alpha);
		} else if(strcmp(alpha, n_alpha) == 0 && strcmp(rest, "0") == 0)
			break;
		if(prevDelta == delta || delta == 0) {
			strcpy(alpha, "");
			break;
		}
	}
	strcpy(*dest, alpha);
}

int main(void) {
	char* n = malloc(sizeof(char) * MAX_LENGTH);
	int succ = scanf("%s", n);
	if(strcmp(n, "0") == 0)
		printf("%s\n", n);
	else {
		char* dest = malloc(sizeof(char) * MAX_LENGTH);
		calc(&n, &dest);
		printf("%s\n", dest);
	}
	return 0;
}