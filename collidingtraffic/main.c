#include <stdio.h>
#include <math.h>

#define RUNTIME 10000
#define RESOLUTION 100000

typedef struct boat {
	long double x;
	long double y;
	long double d;
	long double s;
} boat;

void moveBoats(boat* b, int n) {
	for(int i = 0; i < n; ++i) {
		b[i].x += cos(b[i].d) * (b[i].s / RESOLUTION);
		b[i].y += sin(b[i].d) * (b[i].s / RESOLUTION);
	}
}

void movePair(boat* a, boat*b) {
	b->x += cos(b->d) * (b->s / RESOLUTION);
	b->y += sin(b->d) * (b->s / RESOLUTION);
	a->x += cos(a->d) * (a->s / RESOLUTION);
	a->y += sin(a->d) * (a->s / RESOLUTION);
}

long double conv(long double d) {
	return fmodl((90.0 - d), 360) * (M_PI / 180.0);
}

long double dist(boat* a, boat* b) {
	long double x = pow(b->x - a->x, 2);
	long double y = pow(b->y - a->y, 2);
	// printf("Distance between boats: %llf\n", sqrtl(x + y));
	// printf("Boat1 position: (%llf, %llf)\n", a->x, a->y);
	// printf("Boat2 position: (%llf, %llf)\n", b->x, b->y);
	// printf("---------------------------------\n");
	return sqrtl(x + y);
}

int findCollision(boat* b, int n, long double r) {
	/*
	Computation in this function can be reduced by
	making sure that b only contains intersecting boats somehow
	*/
	long long i;
	int j, k;
	// Loop through i can be reduced by only simulating near collision point
	for(i = 0; i < (long long)RUNTIME * RESOLUTION; ++i) {
		for(j = 0; j < n; ++j) {
			for(k = 0; k < n; ++k) {
				if(j != k) {
					if(dist(&b[j], &b[k]) <= r) {
						long double ret = i / RESOLUTION;
						return (int)(ret < 0 ? (ret - 0.5) : (ret + 0.5));
					}
				}
			}
		}
	}
	return -1;
}

int main(void) {
	int cases, lines, rCases = 0, rLines = 0;
	long double r;
	scanf("%d", &cases);
	while(rCases != cases) {
		scanf("%d %llf", &lines, &r);
		boat boats[lines];
		while(rLines != lines) {
			boat b;
			scanf("%llf %llf %llf %llf", &(b.x), &(b.y), &(b.d), &(b.s));
			b.d = conv(b.d);
			boats[rLines] = b;
			rLines++;
		}
		int res = findCollision(boats, lines, r);
		if(res == -1) printf("No collision.\n"); else {
			printf("%d\n", res);
		}
		rCases++;
		rLines = 0;
	}
	return 0;
}